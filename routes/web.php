<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});




Route::resource('/persons','PersonController');

Route::resource('/students','StudentController');

Route::resource('/students/{id}/delete','StudentController@destroy');




Route::resource('/brands', 'BrandController');

Route::get('/brands/{name}/create','BrandController@store');

Route::get('/brands/{id}','BrandController@show');

Route::get('/brands/{id}/edit','BrandController@edit');

Route::get('/brands/{id}/update','BrandController@update');

Route::get('/brands/{id}/delete','BrandController@destroy');

Route::get('/brands/{id}/relation','BrandController@show_relation');


Route::resource('/products','ProductController');



