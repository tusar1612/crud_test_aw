<?php

$person=new \App\Person();
$alldata=$person->all();
?>

        <!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Bordered Table</h2>
    <p><button class="btn btn-primary">Create</button></p>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Sl</th>
            <th>Name</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>
        <?php
        $sl=0;
        foreach ($alldata as $data){?>
        <tr>
            <td><?php echo $sl++?></td>
            <td><?php echo $data->name?></td>
            <td><a href="/persons/<?php echo $data->id?>/edit"><button class="btn btn-warning" >Edit</button> </a>
               {{--<a href="/"> <button class="btn btn-danger">Delete</button></a></td>--}}
                <form action="/persons/<?php echo $data->id ?>" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <?php echo csrf_field() ?>
                    <button class="btn btn-danger" type="submit" title="Delete" onclick="return confirm('are you want to delete?')">Delete</button>
                </form>
        </tr>

        <?php }?>

        </tbody>
    </table>
</div>

</body>
</html>

