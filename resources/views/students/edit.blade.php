

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Application</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Students inputs form</h2>
    <form action="/students/{{$student->id}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <input name="_method" value="PATCH" type="hidden">
        <div class="form-group">
            <label for="Name">Name:</label>
            <input type="text" class="form-control" name="name" id="Name" placeholder="Enter Name" value="{{$student->name}}">
        </div>
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="Enter email" value="{{$student->email}}">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" name="password" id="pwd" placeholder="Enter password" value="{{$student->password}}">
        </div>
        <div class="form-group">
            <label for="sel1">Select list:</label>
            <select class="form-control"  name="gender" id="sel1">
                <option>Select</option>
                <option value="Male">Male</option>
                <option value="femail">Female</option>

            </select>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="Playing Football" name="hobby[]">Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="Playing cricket" name="hobby[]">Playing cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="Playing chess" name="hobby[]">Playing chess</label>
        </div>
        <label class="btn btn-default btn-file">
            Browse <input type="file" name="upload" value="{{$student->picture}}">
        </label>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>









{
