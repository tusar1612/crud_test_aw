<?php
use App\Student;
$student=new Student();
$alldata=$student->all();
//echo Form::submit('Click Me!');
?>

        <!DOCTYPE html>
<html lang="en">
<head>
    <title>Students Table</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Bordered Table</h2>
    <p><button class="btn btn-primary">Create</button></p>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Sl</th>
            <th>Name</th>
            <th>Email</th>
            <th>gender</th>
            <th>hobbies</th>
            <th>Picture</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>
        <?php
        $sl=1;
        foreach ($alldata as $data){?>
        <tr>
            <td><?php echo $sl++?></td>
            <td><?php echo $data->name?></td>
            <td><?php echo $data->email?></td>
            <td><?php echo $data->gender?></td>
            <td><?php echo $data->hobbies?></td>
            <td><img src="{{url('/uploads/')}}/{{$data->picture}}" width="100px"></td>

            <td>

                <a href="/students/{{$data->id}}/edit"> <button class="btn btn-warning" >Edit</button> </a>
               {{--<a href="/"> <button class="btn btn-danger">Delete</button></a></td>--}}



                <a href="/students/{{$data->id}}/delete"><button class="btn btn-danger" type="submit"  onclick="return confirm('are you want to delete?')">Delete</button></a>
               {{-- <form action="" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE"/>

                </form>--}}

            </td>

        </tr>

        <?php }?>

        </tbody>
    </table>
</div>

</body>
</html>

