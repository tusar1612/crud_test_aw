<?php
$brand=new \App\Brand();
        $alldata=$brand->all();


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Application</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Product Entry</h2>
    <form action="/products" method="post" >
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="form-group">
            <label for="Name">Name:</label>
            <input type="text" class="form-control" name="name" id="Name" placeholder="Enter Name">
        </div>

        <div class="form-group">
            <label for="sel1">Select Brand:</label>
            <select class="form-control"  name="brand_id" id="sel1">
                <option>Select</option>
                @foreach($alldata as $data)
                <option value="{{$data->id}}">{{$data->name}}</option>
                    @endforeach


            </select>
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>








