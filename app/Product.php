<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=['name','brand_id'];

    
    public $timestamps = false;
    public function brand(){

        return $this->belongsTo('App\Brand');
    }
}
